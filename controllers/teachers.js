const { major, teacher } = require("../models");

class Teachers {
  async getAllTeachers(req, res, next) {
    try {
      const data = await teacher.findAll({
        attributes: { exclude: ["major_id"] },
        include: [
          {
            model: major,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });

      if (data.length === 0) {
        return next({ statusCode: 404, messages: ["Teachers not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async getTeacherById(req, res, next) {
    try {
      const data = await teacher.findOne({
        where: { id: req.params.id },
        attributes: {
          exclude: ["major_id", "createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: major,
            attributes: {
              exclude: ["createdAt", "updatedAt", "deletedAt"],
            },
          },
        ],
      });

      if (!data) {
        return next({ statusCode: 404, messages: "Teacher not found" });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async createTeacher(req, res, next) {
    try {
      const newData = await teacher.create(req.body);
      const data = await teacher.findOne({
        where: {
          id: newData.id,
        },
        attributes: { exclude: ["major_id", "updatedAt"] },
        include: [
          {
            model: major,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateTeacher(req, res, next) {
    try {
      const update = await teacher.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      if (update[0] === 0) {
        return next({ statusCode: 404, messages: ["Teacher not found"] });
      }

      const data = await teacher.findOne({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ["major_id", "deletedAt", "createdAt"] },
        include: [
          {
            model: major,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async deleteTeacherById(req, res, next) {
    try {
      const data = await teacher.destroy({
        where: {
          id: req.params.id,
        },
      });

      if (!data) {
        return next({ statusCode: 400, messages: ["Teacher not found"] });
      }

      res.status(200).json({ messages: "Teacher successfully deleted!" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Teachers();
