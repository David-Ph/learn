const { major } = require("../models");

class Majors {
  async getAllMajors(req, res, next) {
    try {
      // get all the data
      const data = await major.findAll();
      // if not found
      if (data.length === 0) {
        return next({ statusCode: 404, messages: "Majors not found" });
      }
      //   send the data back if found!
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async getOneMajor(req, res, next) {
    try {
      const data = await major.findOne({
        where: { id: req.params.id },
      });
      console.log(data);
      if (!data) {
        return next({ statusCode: 404, messages: ["Major not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async createMajor(req, res, next) {
    try {
      const newData = await major.create(req.body);
      console.log(newData);
      const data = await major.findOne({
        where: { id: newData.id },
      });

      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateMajor(req, res, next) {
    try {
      const updateMajor = await major.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      console.log(updateMajor);

      if (updateMajor[0] === 0) {
        return next({ statusCode: 404, messages: ["Major not found"] });
      }

      const data = await major.findOne({
        where: {
          id: req.params.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async deleteMajor(req, res, next) {
    try {
      const data = await major.destroy({
        where: { id: req.params.id },
      });

      if (!data) {
        return next({ statusCode: 404, messages: ["Major not found"] });
      }

      res.status(200).json({ message: "Successfully deleted!" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Majors();
