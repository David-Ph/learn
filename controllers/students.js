const { teacher, student } = require("../models");

class Students {
  async getAllStudents(req, res, next) {
    try {
      const data = await student.findAll({
        attributes: { exclude: ["teacher_id"] },
        include: [
          {
            model: teacher,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });

      if (data.length === 0) {
        return next({ statusCode: 404, messages: ["Students not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async getStudentById(req, res, next) {
    try {
      const data = await student.findOne({
        where: { id: req.params.id },
        attributes: {
          exclude: ["teacher_id", "createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: teacher,
            attributes: {
              exclude: ["createdAt", "updatedAt", "deletedAt"],
            },
          },
        ],
      });

      if (!data) {
        return next({ statusCode: 404, messages: "Student not found" });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async createStudent(req, res, next) {
    try {
      const newData = await student.create(req.body);
      const data = await student.findOne({
        where: {
          id: newData.id,
        },
        attributes: { exclude: ["teacher_id", "updatedAt"] },
        include: [
          {
            model: teacher,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateStudent(req, res, next) {
    try {
      const update = await student.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      if (update[0] === 0) {
        return next({ statusCode: 404, messages: ["Student not found"] });
      }

      const data = await student.findOne({
        where: {
          id: req.params.id,
        },
        attributes: { exclude: ["teacher_id", "deletedAt", "createdAt"] },
        include: [
          {
            model: teacher,
            attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
          },
        ],
      });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async deleteStudentById(req, res, next) {
    try {
      const data = await student.destroy({
        where: {
          id: req.params.id,
        },
      });

      if (!data) {
        return next({ statusCode: 400, messages: ["Student not found"] });
      }

      res.status(200).json({ messages: "Student successfully deleted!" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Students();
