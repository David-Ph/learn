"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class major extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.major.hasMany(models.teacher, {
        foreignKey: "major_id",
      });
    }
  }
  major.init(
    {
      major_name: DataTypes.STRING,
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "major",
    }
  );
  return major;
};
