"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.teacher.belongsTo(models.major, {
        foreignKey: "major_id",
      });
      models.teacher.hasMany(models.student, {
        foreignKey: "teacher_id",
      });
    }
  }
  teacher.init(
    {
      name: DataTypes.STRING,
      major_id: DataTypes.INTEGER,
      age: DataTypes.INTEGER,
      images: {
        type: DataTypes.STRING,
        //Set custom getter for book image using URL
        get() {
          const image = this.getDataValue("images");

          if (!image) {
            return image;
          }

          return "/images/" + image;
        },
      },
    },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      modelName: "teacher",
    }
  );
  return teacher;
};
