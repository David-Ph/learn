"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("teachers", [
      {
        name: "Jane",
        major_id: 1,
        age: 32,
        images: "randomImage.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Bob",
        major_id: 2,
        age: 32,
        images: "randomImage.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Michael",
        major_id: 3,
        age: 32,
        images: "randomImage.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("teachers", null, {});
  },
};
