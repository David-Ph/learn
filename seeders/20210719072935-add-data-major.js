"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("majors", [
      {
        major_name: "Biology",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        major_name: "Math",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        major_name: "Literature",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("majors", null, {});
  },
};
