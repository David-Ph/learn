"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("students", [
      {
        name: "Alice",
        teacher_id: 1,
        age: 17,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Dick",
        teacher_id: 2,
        age: 17,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Kim",
        teacher_id: 3,
        age: 17,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("students", null, {});
  },
};
