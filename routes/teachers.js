const express = require("express");
const router = express.Router();
const Teachers = require("../controllers/teachers");
const { teacherValidator } = require("../middlewares/validator/teachers");

router.get("/", Teachers.getAllTeachers);
router.get("/:id", Teachers.getTeacherById);
router.post("/", teacherValidator, Teachers.createTeacher);
router.put("/:id", teacherValidator, Teachers.updateTeacher);
router.delete("/:id", Teachers.deleteTeacherById);

module.exports = router;
