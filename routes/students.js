const express = require("express");
const router = express.Router();
const Students = require("../controllers/students");
const { studentValidator } = require("../middlewares/validator/students");

router.get("/", Students.getAllStudents);
router.get("/:id", Students.getStudentById);
router.post("/", studentValidator, Students.createStudent);
router.put("/:id", studentValidator, Students.updateStudent);
router.delete("/:id", Students.deleteStudentById);

module.exports = router;
