const express = require("express");
const router = express.Router();
const Majors = require("../controllers/majors");
const { majorValidator } = require("../middlewares/validator/majors");

router.get("/", Majors.getAllMajors);
router.get("/:id", Majors.getOneMajor);
router.post("/", majorValidator, Majors.createMajor);
router.put("/:id", majorValidator, Majors.updateMajor);
router.delete("/:id", Majors.deleteMajor);

module.exports = router;
