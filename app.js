// ? import dependencies
// /////////////////////
const express = require("express");
const app = express();
const fileUpload = require("express-fileupload");

// ? import routes
const errorHandler = require("./middlewares/errorHandler/error");
const MajorsRouter = require("./routes/majors");
const TeachersRouter = require("./routes/teachers");
const StudentsRouter = require("./routes/students");

// ? app.use
// /////////////////////
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());
app.use(express.static("public"));

// ? set up routes
// ////////////////////
app.use("/majors", MajorsRouter);
app.use("/teachers", TeachersRouter);
app.use("/students", StudentsRouter);

app.use(errorHandler);
// ? listening
// /////////////////////
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log("Listening to 3000..."));
