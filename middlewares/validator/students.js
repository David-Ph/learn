const validator = require("validator");
const { teacher } = require("../../models");

exports.studentValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (!validator.isInt(req.body.teacher_id)) {
      errorMessages.push("teacher_id must be a number");
    }

    if (validator.isEmpty(req.body.name)) {
      errorMessages.push("name cannot be empty");
    }

    if (!validator.isInt(req.body.age)) {
      errorMessages.push("age must be a number");
    }

    const findTeacher = await teacher.findOne({
      where: { id: req.body.teacher_id },
    });

    if (!findTeacher) {
      errorMessages.push("Teacher not found");
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};
