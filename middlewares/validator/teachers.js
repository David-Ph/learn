const validator = require("validator");
const { promisify } = require("util");
const { major } = require("../../models");

exports.teacherValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (!validator.isInt(req.body.major_id)) {
      errorMessages.push("major_id must be a number");
    }

    if (validator.isEmpty(req.body.name)) {
      errorMessages.push("name cannot be empty");
    }

    if (!validator.isInt(req.body.age)) {
      errorMessages.push("age must be a number");
    }
    // if image was uploaded
    if (req.files) {
      // req.files.image comes from key(file)
      const file = req.files.images;
      // make sure image is photo
      if (!file.mimetype.startsWith("image")) {
        errorMessages.push("File must be an image");
      }

      // check file size
      if (file.size > 1000000) {
        errorMessages.push("Image must be less than 1MB");
      }

      //   create custom filename
      let fileName = new Date().getTime() + "_" + file.name;
      //   rename the file
      file.name = fileName;

      //   make file.mv to promise
      const move = promisify(file.mv);

      //   uplaod image to /public/images;
      await move(`./public/images/${file.name}`);
      //   assign req.body.image with file.name
      req.body.images = file.name;
    }

    const findMajor = await major.findOne({
      where: { id: req.body.major_id },
    });

    if (!findMajor) {
      errorMessages.push("Major not found");
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, messages: errorMessages });
    }

    next();
  } catch (error) {
    next(error);
  }
};
